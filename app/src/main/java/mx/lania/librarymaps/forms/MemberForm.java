package mx.lania.librarymaps.forms;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import mx.lania.librarymaps.MainActivity;
import mx.lania.librarymaps.R;
import mx.lania.librarymaps.customproviders.MemberProvider;
import mx.lania.librarymaps.db.models.Member;

public class MemberForm extends AppCompatActivity {
    
    private EditText nameTxt;
    private EditText bdTxt;
    private EditText addressTxt;
    private EditText registryTxt;
    private EditText latTxt;
    private EditText longTxt;
    private Button btnAddMemberData;
    private Button btnCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_form);

        //Enlazar componentes view
        nameTxt = (EditText) findViewById(R.id.nameTxt);
        bdTxt = (EditText) findViewById(R.id.bdTxt);
        addressTxt = (EditText) findViewById(R.id.addressTxt);
        registryTxt = (EditText) findViewById(R.id.registryTxt);
        latTxt = (EditText) findViewById(R.id.latTxt);
        longTxt = (EditText) findViewById(R.id.longTxt);
        btnAddMemberData = (Button) findViewById(R.id.btnAddMemberData);
        btnCancel = (Button) findViewById(R.id.btnCancel);

        //Evento del boton
        btnAddMemberData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Insertar miembro
                ContentValues newMemberData = new ContentValues();
                newMemberData.put(Member.NAME_FIELD, nameTxt.getText().toString().trim());
                newMemberData.put(Member.BIRTHDATE_FIELD, bdTxt.getText().toString().trim());
                newMemberData.put(Member.ADDRESS_FIELD, addressTxt.getText().toString().trim());
                newMemberData.put(Member.REGISTRY_FIELD, registryTxt.getText().toString().trim());
                newMemberData.put(Member.LATITUDE_FIELD, latTxt.getText().toString().trim());
                newMemberData.put(Member.LONGITUDE_FIELD, longTxt.getText().toString().trim());

                Uri uri = getContentResolver().insert(MemberProvider.SELECT_CONTENT_URI, newMemberData);
                if(uri != null) {
                    showMsg("Registro agrgado en:\n"+ uri);
                }

                nameTxt.setText("");
                bdTxt.setText("");
                addressTxt.setText("");
                registryTxt.setText("");
                latTxt.setText("");
                longTxt.setText("");
                showMsg("Miembro guardado correctamente");
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MemberForm.this, MainActivity.class);
                startActivity(i);
            }
        });
    }

    public void displayMembers(Cursor c) {
        Toast.makeText(getBaseContext(),
                "ID: " + c.getString(0) + "\n"
                        + "Nombre: " + c.getString(1) + "\n"
                        + "Nacimiento: " + c.getString(2) + "\n"
                        + "Direccion: " + c.getString(3) + "\n"
                        + "Registro: " + c.getString(4) + "\n",
                Toast.LENGTH_LONG).show();
    }

    public void showMsg(String msg){
        Toast.makeText(getBaseContext(), msg, Toast.LENGTH_SHORT).show();
    }
}