package mx.lania.librarymaps.forms;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import mx.lania.librarymaps.R;
import mx.lania.librarymaps.customproviders.MemberProvider;
import mx.lania.librarymaps.db.models.Member;

public class EditDeleteMember extends AppCompatActivity implements View.OnClickListener{

    private Button btnUpdateMember;
    private Button btnDeleteMember;
    private Button btnSeekMember;
    private EditText nameTxt;
    private EditText bdTxt;
    private EditText addressTxt;
    private EditText registryTxt;
    private EditText latTxt;
    private EditText longTxt;
    private TextView tvBD;
    private TextView tvAddress;
    private TextView tvRegistry;
    private TextView tvData;
    private TextView tvLat;
    private TextView tvLong;
    private long _id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_delete_member);

        //Enlazar view
        btnDeleteMember = (Button) findViewById(R.id.btnDeleteMember);
        btnUpdateMember = (Button) findViewById(R.id.btnUpdateMember);
        btnSeekMember = (Button) findViewById(R.id.btnSeekMember);

        nameTxt = (EditText) findViewById(R.id.nameTxt);
        bdTxt = (EditText) findViewById(R.id.bdTxt);
        addressTxt = (EditText) findViewById(R.id.addressTxt);
        registryTxt = (EditText) findViewById(R.id.registryTxt);
        latTxt = (EditText) findViewById(R.id.latTxt);
        longTxt = (EditText) findViewById(R.id.longTxt);

        tvBD = (TextView) findViewById(R.id.tvBD);
        tvAddress = (TextView) findViewById(R.id.tvAddress);
        tvRegistry = (TextView) findViewById(R.id.tvRegistry);
        tvData = (TextView) findViewById(R.id.tvData);
        tvLat = (TextView) findViewById(R.id.tvLat);
        tvLong = (TextView) findViewById(R.id.tvLong);

        //Deshabilitar cuados de texto
        enableTxtFields(false);

        //Evento click
        btnDeleteMember.setOnClickListener(this);
        btnUpdateMember.setOnClickListener(this);
        btnSeekMember.setOnClickListener(this);

        //inicializacion del _id
        _id = -1;


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSeekMember:
                seekMember();
                break;
            case R.id.btnDeleteMember:
                deleteMember();
                break;
            case R.id.btnUpdateMember:
                updateMember();
                break;
        }
    }

    //Metodo para buscar miembro
    public void seekMember(){
        String seekM = nameTxt.getText().toString().trim();
        Cursor c = getContentResolver().query(Uri.parse(MemberProvider.SELECT_BY_NAME_URL + seekM), null, null, null, null);
        if (c.moveToFirst()){
            do {
                enableTxtFields(true);
                _id = c.getLong(0);
                bdTxt.setText(c.getString(2));
                addressTxt.setText(c.getString(3));
                registryTxt.setText(c.getString(4));
                latTxt.setText(c.getString(5));
                longTxt.setText(c.getString(6));
            } while (c.moveToNext());
        } else showMsg("No se ha encontrado el miembro");
    }

    //Metodo para eliminar miembro
    public void deleteMember(){
        if(_id != -1){
            int rowAffected = getContentResolver().delete(Uri.parse(MemberProvider.SELECT_BY_ID_URL + _id),null, null);

            if(rowAffected > 0) {
                showMsg("Elemento eliminado");
                cleanFields();
                _id = -1;
                enableTxtFields(false);
            } else showMsg("Elemento no concontrado");
        }
    }

    //Metodo para actualizar miembro
    public void updateMember(){
        if(_id != -1){
            ContentValues newMemberData = new ContentValues();
            newMemberData.put(Member.NAME_FIELD, nameTxt.getText().toString().trim());
            newMemberData.put(Member.BIRTHDATE_FIELD, bdTxt.getText().toString().trim());
            newMemberData.put(Member.ADDRESS_FIELD, addressTxt.getText().toString().trim());
            newMemberData.put(Member.REGISTRY_FIELD, registryTxt.getText().toString().trim());
            newMemberData.put(Member.LATITUDE_FIELD, latTxt.getText().toString().trim());
            newMemberData.put(Member.LONGITUDE_FIELD, longTxt.getText().toString().trim());

            int rowAffected = getContentResolver().update(Uri.parse(MemberProvider.SELECT_BY_ID_URL + _id), newMemberData, null, null);
            if(rowAffected > 0) {
                showMsg("Elemento Actualizado");
                cleanFields();
                _id = -1;
                enableTxtFields(false);
            } else showMsg("Elemento no concontrado");
        }
    }

    //Mostrar mensaje
    public void showMsg(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    //Control de cuadros de texto
    private void enableTxtFields(boolean flag){
        bdTxt.setEnabled(flag);
        addressTxt.setEnabled(flag);
        registryTxt.setEnabled(flag);
        btnUpdateMember.setEnabled(flag);
        btnDeleteMember.setEnabled(flag);
        if(flag){
            bdTxt.setVisibility(View.VISIBLE);
            addressTxt.setVisibility(View.VISIBLE);
            registryTxt.setVisibility(View.VISIBLE);
            latTxt.setVisibility(View.VISIBLE);
            longTxt.setVisibility(View.VISIBLE);
            btnUpdateMember.setVisibility(View.VISIBLE);
            btnDeleteMember.setVisibility(View.VISIBLE);
            tvBD.setVisibility(View.VISIBLE);
            tvAddress.setVisibility(View.VISIBLE);
            tvRegistry.setVisibility(View.VISIBLE);
            tvData.setVisibility(View.VISIBLE);
            tvLat.setVisibility(View.VISIBLE);
            tvLong.setVisibility(View.VISIBLE);
        }else{
            bdTxt.setVisibility(View.INVISIBLE);
            addressTxt.setVisibility(View.INVISIBLE);
            registryTxt.setVisibility(View.INVISIBLE);
            latTxt.setVisibility(View.INVISIBLE);
            longTxt.setVisibility(View.INVISIBLE);
            btnUpdateMember.setVisibility(View.INVISIBLE);
            btnDeleteMember.setVisibility(View.INVISIBLE);
            tvBD.setVisibility(View.INVISIBLE);
            tvAddress.setVisibility(View.INVISIBLE);
            tvRegistry.setVisibility(View.INVISIBLE);
            tvData.setVisibility(View.INVISIBLE);
            tvLat.setVisibility(View.INVISIBLE);
            tvLong.setVisibility(View.INVISIBLE);
        }
    }

    //Limpiar datos
    private void cleanFields(){
        bdTxt.setText("");
        addressTxt.setText("");
        registryTxt.setText("");
        nameTxt.setText("");
        latTxt.setText("");
        longTxt.setText("");
    }
}