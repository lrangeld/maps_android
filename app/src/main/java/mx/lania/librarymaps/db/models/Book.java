package mx.lania.librarymaps.db.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import mx.lania.librarymaps.db.helpers.DatabaseHelper;

public class Book {

    //Campos de tabla
    public static final String ID_FIELD = "_id";
    public static final String TITLE_FIELD = "titulo";
    public static final String AVAILABILITY_FIELD = "disponibilidad";
    public static final String PRICE_FIELD = "precio";
    public static final String AUTHOR_FIELD = "autor";

    //Informacion de la tabla
    public static final String TABLE_NAME = "libros";

    //Consulta de eliminacion
    public static final String BOOK_DROP_TABLE = "DROP TABLE IF EXISTS "+ TABLE_NAME;

    //Creacion de la tabla
    public static final String BOOK_TABLE_CREATE = "create table "+ TABLE_NAME +" ("
            + ID_FIELD + " integer primary key autoincrement, "
            + TITLE_FIELD + " text not null, "
            + AVAILABILITY_FIELD + " text not null, "
            + PRICE_FIELD + " text not null, "
            + AUTHOR_FIELD + " text not null);";

    final Context context;
    DatabaseHelper DBHelper;
    public SQLiteDatabase db;

    //Contructor de clase
    public Book(Context ctx) {
        this.context = ctx;
        DBHelper = new DatabaseHelper(context);
        Log.e("LOG", BOOK_TABLE_CREATE);
    }

    //Abrir conexion
    public Book openConnection() throws SQLException {
        db = DBHelper.getWritableDatabase();
        return this;
    }

    //Cerrar conexion
    public void closeConnection() {
        DBHelper.close();
    }

    //Insertar un nuevo libro
    public long insertBook(String title, String availability, String price, String author) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(TITLE_FIELD, title);
        initialValues.put(AVAILABILITY_FIELD, availability);
        initialValues.put(PRICE_FIELD, price);
        initialValues.put(AUTHOR_FIELD, author);
        return db.insert(TABLE_NAME, null, initialValues);
    }

    //Obtener todos los libros
    public Cursor getBooks() {
        return db.query(TABLE_NAME, new String[]{ID_FIELD, TITLE_FIELD, AVAILABILITY_FIELD, PRICE_FIELD, AUTHOR_FIELD},
                null, null, null, null, null);
    }


    //Obtener un libro
    public Cursor getBook(long _id) throws SQLException {
        Cursor cursor = db.query(true,
                TABLE_NAME,
                new String[]{ID_FIELD, TITLE_FIELD, AVAILABILITY_FIELD, PRICE_FIELD, AUTHOR_FIELD},
                ID_FIELD + "=" + _id,
                null,
                null,
                null,
                null,
                null);
        if (cursor != null)
            cursor.moveToFirst();
        return cursor;
    }


    //Actualizar libro
    public boolean updateBook(long _id, String title, String availability, String price, String author) {
        ContentValues args = new ContentValues();
        args.put(TITLE_FIELD, title);
        args.put(AVAILABILITY_FIELD, availability);
        args.put(PRICE_FIELD, price);
        args.put(AUTHOR_FIELD, author);
        return db.update(TABLE_NAME, args, ID_FIELD + "=" + _id, null) > 0;
    }

    //Borrar libro
    public boolean deleteBook(long _id) {
        return db.delete(TABLE_NAME, ID_FIELD + "=" + _id, null) > 0;
    }
}
