package mx.lania.librarymaps.db.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import mx.lania.librarymaps.db.helpers.DatabaseHelper;

public class Editor {

    //Campos de tabla
    static final String ID_FIELD = "_id";
    static final String NAME_FIELD = "nombre";
    static final String ADDRESS_FIELD = "direccion";

    //Informacion de la tabla
    static final String TABLE_NAME = "editores";

    //Consulta de eliminacion
    public static final String EDITOR_DROP_TABLE = "DROP TABLE IF EXISTS "+ TABLE_NAME;

    //Creacion de la tabla
    public static final String EDITOR_TABLE_CREATE = "create table "+ TABLE_NAME +" ("
            + ID_FIELD + " integer primary key autoincrement, "
            + NAME_FIELD + " text not null, "
            + ADDRESS_FIELD + " text not null);";

    final Context context;
    DatabaseHelper DBHelper;
    SQLiteDatabase db;

    //Contructor de clase
    public Editor(Context ctx) {
        this.context = ctx;
        DBHelper = new DatabaseHelper(context);
        Log.e("LOG", EDITOR_TABLE_CREATE);
    }

    //Abrir conexion
    public Editor openConnection() throws SQLException {
        db = DBHelper.getWritableDatabase();
        return this;
    }

    //Cerrar conexion
    public void closeConnection() {
        DBHelper.close();
    }

    //Insertar un nuevo editor
    public long insertEditor(String name, String address) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(NAME_FIELD, name);
        initialValues.put(ADDRESS_FIELD, address);
        return db.insert(TABLE_NAME, null, initialValues);
    }

    //Obtener todos los editor
    public Cursor getEditors() {
        return db.query(TABLE_NAME, new String[]{ID_FIELD, NAME_FIELD, ADDRESS_FIELD},
                null, null, null, null, null);
    }


    //Obtener un editor
    public Cursor getEditor(long _id) throws SQLException {
        Cursor cursor = db.query(true,
                TABLE_NAME,
                new String[]{ID_FIELD, NAME_FIELD, ADDRESS_FIELD},
                ID_FIELD + "=" + _id,
                null,
                null,
                null,
                null,
                null);
        if (cursor != null)
            cursor.moveToFirst();
        return cursor;
    }

    //Actualizar editor
    public boolean updateEditor(long _id, String name, String address) {
        ContentValues args = new ContentValues();
        args.put(NAME_FIELD, name);
        args.put(ADDRESS_FIELD, address);
        return db.update(TABLE_NAME, args, ID_FIELD + "=" + _id, null) > 0;
    }

    //Borrar editor
    public boolean deleteEditor(long _id) {
        return db.delete(TABLE_NAME, ID_FIELD + "=" + _id, null) > 0;
    }
}


