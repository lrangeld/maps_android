package mx.lania.librarymaps.db.helpers;

import android.database.sqlite.SQLiteOpenHelper;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import mx.lania.librarymaps.db.models.Book;
import mx.lania.librarymaps.db.models.Editor;
import mx.lania.librarymaps.db.models.Member;

public class DatabaseHelper extends SQLiteOpenHelper {

    static final int DATABASE_VERSION = 1;
    static final String DATABASE_NAME = "Libreria";
    public static final String TAG = "SQLite_Log";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(Book.BOOK_TABLE_CREATE);
            db.execSQL(Member.MEMBER_TABLE_CREATE);
            db.execSQL(Editor.EDITOR_TABLE_CREATE);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(TAG, "Actualizando base de datos de la version " + oldVersion + " a " + newVersion + ", toda la informacion almacenada sera borrada");
        db.execSQL(Book.BOOK_DROP_TABLE);
        db.execSQL(Member.MEMBER_DROP_TABLE);
        db.execSQL(Editor.EDITOR_DROP_TABLE);
        onCreate(db);
    }
}
