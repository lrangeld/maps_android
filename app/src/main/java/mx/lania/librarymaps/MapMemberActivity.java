package mx.lania.librarymaps;

import android.Manifest;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import mx.lania.librarymaps.customproviders.MemberProvider;

public class MapMemberActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private GoogleMap mMap;
    private ArrayList<MemberData> Members;
    private static final LatLng MEXICO_LAT_LNG = new LatLng(23.634501,  -102.552784);
    private static final float DEFAULT_ZOOM = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_member);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.setOnMarkerClickListener(this);
        getMembersData();

        for (MemberData m: Members) {
            mMap.addMarker(new MarkerOptions().position(new LatLng(m.latitude, m.longitude)).title(m.name));
        }
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(MEXICO_LAT_LNG, DEFAULT_ZOOM));
    }

    private void getMembersData(){
        this.Members = new ArrayList<>();
        Cursor c = getContentResolver().query(MemberProvider.SELECT_CONTENT_URI, null, null, null, null);

        if (c.moveToFirst())
            do {
                this.Members.add(new MemberData(
                        Double.parseDouble(c.getString(0)),
                        Double.parseDouble(c.getString(1)),
                        c.getString(6),
                        c.getString(4)
                ));
            } while (c.moveToNext());
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        marker.showInfoWindow();
        new LocationAsynctask().execute(marker.getPosition());
        return true;
    }


    //Clase privada para realizar la localizacion del marcador
    private class LocationAsynctask extends AsyncTask<LatLng, Void, Address> {
        @Override
        protected Address doInBackground(LatLng... params) {
            LatLng latLng = params[0];
            Geocoder geocoder = new Geocoder(MapMemberActivity.this, Locale.getDefault());
            List<Address> addresses = null;
            try {
                addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
                return !addresses.isEmpty() ? addresses.get(0) : null;
            } catch (IOException ioException) {
                showMsg(ioException.getMessage(), Toast.LENGTH_SHORT);
            } catch (IllegalArgumentException illegalArgumentException) {
                showMsg(illegalArgumentException.getMessage(), Toast.LENGTH_SHORT);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Address addressObj) {
            StringBuilder address = new StringBuilder();
            for (int i = 0; i <= addressObj.getMaxAddressLineIndex(); i++) {
                address.append(addressObj.getAddressLine(i)).append("\n");
            }
            showMsg("Direccion: " + address, Toast.LENGTH_LONG);
        }
    }

    //Clase privada para encapsulacion de datos
    private class MemberData{
        private double latitude;
        private double longitude;
        private String name;
        private String address;

        public MemberData(double latitude, double longitude, String name, String address) {
            this.latitude = latitude;
            this.longitude = longitude;
            this.name = name;
            this.address = address;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }
    }

    private void showMsg(String msg, int duration){
        Toast.makeText(MapMemberActivity.this, msg, duration).show();
    }
}